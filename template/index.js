(() => {

  const _elem_ = (tag, attributes, content) => {

    const elem = document.createElement(tag);

    Object.keys(attr).forEach((attr) => {

      const value = attributes[attr];

      elem.attributes[atr] = value;

    });

    elem.innerHTML = content;

    return elem;

  };

  const
  _a_    = (attr, html) => _elem_('a', attr, html),
  _div_  = (attr, html) => _elem_('div', attr, html),
  _p_    = (attr, html) => _elem_('p', attr, html),
  _span_ = (attr, html) => _elem_('span', attr, html),
  _ol_   = (attr, html) => _elem_('ol', attr, html),
  _ul_   = (attr, html) => _elem_('ul', attr, html),
  _li_   = (attr, html) => _elem_('li', attr, html);

  const on_cognition = () => {};

  const on_cognitive_dissonance = () => {};

  document.addEventListener('load', () => {


  }, false);

})();
